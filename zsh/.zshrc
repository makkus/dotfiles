# Source Znap at the start of your .zshrc file.
source ~/.local/setup/git_repos/zsh/zsh-snap/znap.zsh

alias gdircolors=dircolors

# `znap prompt` makes your prompt appear in ~40ms. You can start typing right away!
znap prompt denysdovhan/spaceship-prompt

# ==================================================
# source common functions, vars, aliases
source "${HOME}/.local/setup/dotfiles/common/shell/functions.sh"
source "${HOME}/.local/setup/dotfiles/common/shell/vars.sh"
source "${HOME}/.local/setup/dotfiles/common/shell/aliases.sh"

# ==================================================
# history stuff
HISTFILE="$HOME/.zsh_history"
HISTSIZE=10000000
SAVEHIST=10000000
setopt BANG_HIST                 # Treat the '!' character specially during expansion.
setopt EXTENDED_HISTORY          # Write the history file in the ":start:elapsed;command" format.
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.
setopt SHARE_HISTORY             # Share history between all sessions.
setopt HIST_EXPIRE_DUPS_FIRST    # Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_DUPS          # Don't record an entry that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS      # Delete old recorded entry if new entry is a duplicate.
setopt HIST_FIND_NO_DUPS         # Do not display a line previously found.
setopt HIST_IGNORE_SPACE         # Don't record an entry starting with a space.
setopt HIST_SAVE_NO_DUPS         # Don't write duplicate entries in the history file.
setopt HIST_REDUCE_BLANKS        # Remove superfluous blanks before recording entry.
setopt HIST_VERIFY               # Don't execute immediately upon history expansion.
setopt HIST_BEEP                 # Beep when accessing nonexistent history.


# Now your prompt is visible and you can type, even though your .zshrc file hasn't finished loading
# yet! In the background, the rest of your `.zshrc` file continues to be executed.
# Use `znap source` to load only those parts of Oh-My-Zsh or Prezto that you really need:
znap source ohmyzsh/ohmyzsh plugins/git

# znap source sorin-ionescu/prezto modules/{environment,history}
# `znap prompt` also supports Oh-My-Zsh themes. Just make sure you load the required libs first:
# znap source ohmyzsh/ohmyzsh lib/{git,theme-and-appearance}
# znap prompt ohmyzsh/ohmyzsh robbyrussell
znap source ajeetdsouza/zoxide

# fzf
_fzf_compgen_path() {
  fd --hidden --follow --exclude ".git" . "$1"
}
# Use fd to generate the list for directory completion
_fzf_compgen_dir() {
  fd --type d --hidden --follow --exclude ".git" . "$1"
}
export FZF_DEFAULT_COMMAND='fd --type f --hidden --follow --exclude .git'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_CTRL_R_OPTS='--sort'
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

zstyle ':autocomplete:tab:*' widget-style menu-select
zstyle ':autocomplete:tab:*' insert-unambiguous yes
zstyle ':autocomplete:*' min-delay .15
zstyle ':autocomplete:*' min-input 1
zstyle ':autocomplete:tab:*' fzf-completion yes

# Use `znap source` to load your plugins:
znap source marlonrichert/zsh-autocomplete
znap source marlonrichert/zsh-edit
#znap source zsh-users/zsh-history-substring-search zsh-history-substring-search.zsh

znap source asdf-vm/asdf asdf.sh
# Note that if a plugin requires additional config, there is nothing special you need to do. Just
# use normal Zsh syntax:
znap source marlonrichert/zsh-hist
bindkey '^[q' push-line-or-edit

export ZSH_HIGHLIGHT_HIGHLIGHTERS=( main brackets )
znap source zsh-users/zsh-syntax-highlighting
# Use `znap clone` to download repos that aren't plugins. All downloads in the same call will occur
# in parallel. Any repos you already have will be skipped silently.
znap clone \
  asdf-community/asdf-direnv \
  ekalinin/github-markdown-toc \
  trapd00r/LS_COLORS
# All repos managed by Znap are automatically available as dynamically-named dirs. This makes it
# easier to add commands to your `$path`...
export -U path=(
  ~[github-markdown-toc]
  $path
  .
)
# ...or functions to your `$fpath`.
export -U fpath=(
  ~[asdf]/completions
  $fpath
)
# Likewise, you can also do `cd ~[github-markdown-toc]` or `ls ~[asdf]/completions` to access a
# repo or its contents from any location. In addition, your plugins dir itself can be accessed with
#`cd ~znap` or `ls ~znap`. Try it on the command line!
# Use `zna zop eval` to cache the output of slow commands:
# If the first arg is a repo, then the command will run inside it. Plus, whenever you update the
# repo with `znap pull`, the cache automatically gets regenerated.
znap eval LS_COLORS 'gdircolors -b LS_COLORS'
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
# Here we include the full path to `direnv` in the command string, so that the cache will be
# regenerated whenever the version of `direnv` changes.
znap eval asdf-direnv "asdf exec $(asdf which direnv) hook zsh"
# These don't belong to any repo, but the first arg will be used to name the cache file.
# znap eval brew-shellenv 'brew shellenv'
# znap eval pyenv-init 'pyenv init -'
# znap eval pipenv-completion 'pipenv --completion'
# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/markus/.local/opt/conda/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/markus/.local/opt/conda/etc/profile.d/conda.sh" ]; then
        . "/home/markus/.local/opt/conda/etc/profile.d/conda.sh"
    else
        export PATH="/home/markus/.local/opt/conda/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<


#source /home/markus/projects/dharpa/kiara/.kiara_complete.zsh