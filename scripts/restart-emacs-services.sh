#!/usr/bin/env bash

systemctl --user restart emacs.service
systemctl --user restart emacs-mail.service
systemctl --user restart emacs-pager.service
