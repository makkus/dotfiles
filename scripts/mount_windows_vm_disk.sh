#!/usr/bin/env bash
set -euo pipefail

# Check permissions
if [[ "$UID" != "0" ]]; then
    echo "Need Root Permissions" 2>/dev/null
    exit 1
fi

modprobe loop
modprobe linear

# Disk config
if [[ "$1" == "stop" ]]; then
    # stop virtual RAID disk
    mdadm --stop --scan
    losetup --detach-all
    exit 0
else
	# setup virtual RAID disk
    LOOP1=$(losetup -f)
    losetup ${LOOP1}  /home/markus/.local/vm/efi1
    LOOP2=$(losetup -f)
    losetup ${LOOP2}  /home/markus/.local/vm/efi2
    mdadm --build --verbose /dev/md0 --chunk=512 --level=linear --raid-devices=3 ${LOOP1} /dev/nvme0n1p3 ${LOOP2}
    sleep 1
    chown markus:markus /dev/md0
fi
