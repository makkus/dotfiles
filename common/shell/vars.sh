# ==================================================
# exported environment variables

# editor

export ALTERNATE_EDITOR=""
export EDITOR="emacsclient"
export VISUAL="emacsclient -c"

# pager

# less (from: https://www.topbug.net/blog/2016/09/27/make-gnu-less-more-powerful/ )
export LESS='--quit-if-one-screen --ignore-case --status-column --LONG-PROMPT --RAW-CONTROL-CHARS --HILITE-UNREAD --tabs=4 --no-init --window=-4'
export LESS_TERMCAP_mb=$'\E[1;31m'     # begin bold
export LESS_TERMCAP_md=$'\E[1;36m'     # begin blink
export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
export LESS_TERMCAP_so=$'\E[01;44;33m' # begin reverse video
export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
export LESS_TERMCAP_ue=$'\E[0m'        # reset underline

USER_PATH="$HOME/.local/setup/dotfiles/scripts:$HOME/.local/bin:$HOME/.local/bring:$HOME/.local/opt/emacs/bin:$HOME/.local/apps:$HOME/.local/opt/kitty/bin::$HOME/.krew/bin:$HOME/.local/opt/adb/platform-tools:$HOME/.local/opt/heimdall:/opt/zotero:$HOME/.local/setup/git_repos/rofi-pass:$HOME/.local/setup/git_repos/doom-emacs/bin:$HOME/.emacs.d/bin"

GIT_REPOS_BASE_PATH="$HOME/.local/setup/git_repos"
GIT_REPOS_PATH="$GIT_REPOS_BASE_PATH/rofi-pass:$GIT_REPOS_BASE_PATH/transcrypt"

export PATH="${USER_PATH}:${GIT_REPOS_PATH}:${PATH}"


