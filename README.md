
# base packages to install

    emacs-gtk
    curl
    git
    stow
    zsh
    ripgrep
    htop
    pwgen
    libsecret-tools
    python3-psutil
    python3-pip
    python3-venv
    gtk-redshift
    language-pack-de
    fd-find
    # espanso
    libxtst6 
    libxdo3 
    xclip 
    libnotify-bin
    nmap
    ncal

# snaps

   sudo snap install espanso --classic

# virt

    qemu-kvm
    libvirt-daemon-system
    libvirt-clients
    bridge-utils
    virt-manager
    
## group mgmt

    sudo adduser `id -un` libvirt
    sudo adduser `id -un` kvm
    
# espanso

    espanso install basic-emojis
    espanso install lorem
    espanso install shruggie
    espanso install arrows
    
# regolith (blocks)

    sudo apt install i3xrocks-nm-vpn i3xrocks-keyboard-layout i3xrocks-volume i3xrocks-temp i3xrocks-memory i3xrocks-focused-window-name i3xrocks-disk-capacity i3xrocks-bluetooth i3xrocks-battery


# install asciidoc

    sudo apt-get --no-install-recommends install asciidoc

# install language support
    
    sudo apt install $(check-language-support)
    
# anyconnect

    sudo apt install openconnect network-manager-openconnect network-manager-openconnect-gnome vpnc-scripts

# dev packages

    direnv
    editorconfig
    libssl-dev
    meld
    texlive-latex-recommended
    texlive-latex-extra
    pandoc
    jq
    shellcheck
    
# python dependencies

    libbz2-dev
    libsqlite3-dev
    libreadline-dev
    libffi-dev
    liblzma-dev
    lzma
        
# desktop packages

    thunderbird
    wmctrl
    xdotool
    feh
    pasystray
    blueman
    pavucontrol
    pavumeter
    paprefs
    mpv
    dunst
    kitty

# other


## asdf

    git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.8.0
    
### asdf plugins

    python
    direnv
    nim
    java
    nodejs

## emacs

    # TODO: emacs-doom setup
    sudo apt install emacs-lucid
    git clone <https://github.com/syl20bnr/spacemacs> ~/.emacs.d
    cd .emacs.d
    git checkout develop
    
## fzf

    git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
    ~/.fzf/install

## desktop

### regolith

    sudo add-apt-repository ppa:regolith-linux/release
    sudo apt install regolith-desktop i3xrocks-net-traffic i3xrocks-cpu-usage i3xrocks-time
    sudo apt install regolith-look-nord
    sudo apt install gstreamer1.0-plugins-{bad,base,good,ugly}

## development

### pipx

    python3 -m pip install --user pipx
    python3 -m pipx ensurepath
    

### pipx packages:

    'black[d]'
    pre-commit
    mu-repo
    isort
    pyflakes
    pipenv
    pytest
    nose
    cookiecutter
    pywal

## misc


### yubikey

    sudo apt install -y yubikey-manager yubikey-personalization
    sudo apt install gnupg pcscd scdaemon


### signal

    curl -s https://updates.signal.org/desktop/apt/keys.asc | sudo apt-key add -
    echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list
    sudo apt update && sudo apt install -y signal-desktop

### wire

    wget -q <https://wire-app.wire.com/linux/releases.key> -O- | sudo apt-key add -
    echo "deb [arch=amd64] <https://wire-app.wire.com/linux/debian> stable main" | sudo tee /etc/apt/sources.list.d/wire-desktop.list
    sudo apt-get update && sudo apt-get install -y wire-desktop


TODO


# Folders

.fonts


# gnome settings

# language support

    gsettings set org.gnome.desktop.input-sources sources "[('xkb', 'us'), ('xkb', 'de')]"
    
# key to toggle keyboard layout 

    gsettings set org.gnome.desktop.wm.keybindings switch-input-source  "['<Primary>BackSpace']"
    gsettings set org.gnome.desktop.wm.keybindings switch-input-source-backward "['<Primary><Shift>BackSpace']"


## set fonts

    gsettings set org.gnome.desktop.interface font-name "Roboto 10"
    gsettings set org.gnome.desktop.interface document-font-name "Roboto 10"
    gsettings set org.gnome.desktop.interface monospace-font-name "Noto Mono 10"

## show all input settings in control center

    gsettings set org.gnome.desktop.input-sources show-all-sources true


## set emacs gnome keybindings

    gsettings set org.gnome.desktop.interface gtk-key-theme 'Emacs'


## make Caps Lock an additional Super & menu as right ctrl

    gsettings set org.gnome.desktop.input-sources xkb-options "['caps:super', 'ctrl:menu_rctrl']"

## disable Super-l

    gsettings set org.gnome.settings-daemon.plugins.media-keys screensaver "@as []"

# mail setup

## store password in keyring

    secret-tool store --label="Posteo account password" type email provider posteo username whatever@posteo.de

## create mbsync config for account

File content:

IMAPAccount posteo
Host posteo.de
User whatever@posteo.de
PassCmd "secret-tool lookup type email provider posteo username whatever@posteo.de"
Port 993
SSLTYPE IMAPS
\#SSLVersions SSLv3
CertificateFile /etc/ssl/certs/ca-certificates.crt

Imapstore posteo-remote
Account posteo

MaildirStore posteo-local
Path *mnt/space/storage/mail/posteo*
Inbox /mnt/space/storage/mail/posteo/INBOX
SubFolders Verbatim
Trash Trash
TrashNewonly yes

Channel posteo-inbox
Master :posteo-remote:
Slave :posteo-local:
Patterns INBOX
Create Both
Sync All
Expunge Both
SyncState \*

Channel posteo-drafts
Master :posteo-remote:
Slave :posteo-local:
Patterns Drafts
Create Both
Sync All
Expunge Both
SyncState \*

Channel posteo-folders
Master :posteo-remote:
Slave :posteo-local:
Patterns Lists/\*
Create Both
Sync All
Expunge Both
SyncState \*

Channel posteo-sent
Master :posteo-remote:
Slave :posteo-local:
Patterns Sent
Create Both
Sync All
Expunge Both
SyncState \*

Channel posteo-trash
Master :posteo-remote:
Slave :posteo-local:
Patterns Trash
Create Both
Sync All
Expunge Both
SyncState \*

Channel posteo-archives
Master :posteo-remote:
Slave :posteo-local:
Patterns Archives/2020 Archives/2018 Archives/2017 Archives/2016 Archives/2015 Archives/2015/\*
Create Both
Sync All
Expunge Both
SyncState \*

