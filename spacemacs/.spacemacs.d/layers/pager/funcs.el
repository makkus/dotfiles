;;; funcs.el ---                                     -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Markus Binsteiner

;; Author: Markus Binsteiner <markus@t440p>
;; Keywords:

(defun open-kitty-buffer (path)
  "create a new pager buffer, insert txt"
  (pop-to-buffer (get-buffer-create (generate-new-buffer-name "kitty-pager")))
  (delete-other-windows)
  (insert-file-contents path)
  (ansi-color-apply-on-region (point-min) (point-max))
  (view-mode)
  (local-set-minor-mode-key 'view-mode (kbd "q") 'close-kitty-buffer)
  (local-set-minor-mode-key 'view-mode (kbd "s") 'helm-swoop)
  (local-set-minor-mode-key 'view-mode (kbd "M-,") 'beginning-of-buffer)
  (local-set-minor-mode-key 'view-mode (kbd "M-.") 'end-of-buffer)
  )


(defun close-kitty-buffer ()
  "kill current buffer"
  (interactive)
  (kill-this-buffer)
  (delete-frame)
  )

(defun local-set-minor-mode-key (mode key def)
  "Overrides a minor mode keybinding for the local
   buffer, by creating or altering keymaps stored in buffer-local
   `minor-mode-overriding-map-alist'."
  (let* ((oldmap (cdr (assoc mode minor-mode-map-alist)))
         (newmap (or (cdr (assoc mode minor-mode-overriding-map-alist))
                     (let ((map (make-sparse-keymap)))
                       (set-keymap-parent map oldmap)
                       (push `(,mode . ,map) minor-mode-overriding-map-alist) 
                       map))))
    (define-key newmap key def)))
