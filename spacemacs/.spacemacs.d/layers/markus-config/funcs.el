;;; funcs.el --- Helper functions                    -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Markus Binsteiner

;; Author: Markus Binsteiner <markus@frkl.io>
;; Keywords: lisp

(defun markus/avy-goto-end-of-word-or-subword-1 ()
  "Move point to end of specified word or subword."
  (interactive)
  (call-interactively 'avy-goto-word-or-subword-1)
  (forward-word)
  )

(defun markus/avy-goto-end-of-line ()
  "Move point to end of specified line."
  (interactive)
  (call-interactively 'avy-goto-line)
  (end-of-line)
  )

(defun markus/comment-or-uncomment-region-or-line ()
  "Comments or uncomments the region or the current line if there's no active region."
  (interactive)
  (let (beg end)
    (if (region-active-p)
        (setq beg (region-beginning) end (region-end))
      (setq beg (line-beginning-position) end (line-end-position)))
    (comment-or-uncomment-region beg end)
    (next-line)))

